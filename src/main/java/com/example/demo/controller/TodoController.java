package com.example.demo.controller;

import com.example.demo.model.TodoItem;
import com.example.demo.repo.TodoRepo;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/todo")
public class TodoController {

    @Autowired
    private TodoRepo todoRepo;

    @GetMapping
    private List<TodoItem> findAll(){
        return todoRepo.findAll();
    }

    @PostMapping
    public TodoItem save(@Valid @NotNull @RequestBody TodoItem todoItem) {return todoRepo.save(todoItem);
    }

    @PutMapping
    public TodoItem update (@Valid @NotNull @RequestBody TodoItem todoItem) {return todoRepo.save(todoItem);
    }

    @DeleteMapping(value = "/{id}")
    public void delete (@PathVariable Long id){
        todoRepo.deleteById(id);
    }

}
